const path = require('path');
const APP_DIR_JS = path.resolve(__dirname, './app/js');
const APP_DIR_STYLES = path.resolve(__dirname, './app/styles');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  context: __dirname + '/app',
  entry: {
    'bundle' : './js/main.js',
    'style' : './styles/main.less'
  },

  output: {
    path: path.resolve(__dirname, 'www/assets'),
    publicPath: '',
    filename: '[name].js',
  },

  devtool: 'source-map',

  devServer: {
    contentBase: path.resolve(__dirname, 'www'),
    historyApiFallback: true,
    publicPath: '/assets/'
  },

  module : {
    loaders : [
      { test : /\.js$/, include : APP_DIR_JS, loader : 'babel-loader' },
      { test: /\.less$/, include : APP_DIR_STYLES, loader: ExtractTextPlugin.extract({
        use : [ { loader : 'css-loader', options: { sourceMap: true } }, { loader : 'less-loader', options: { sourceMap: true } } ]
      }) },
      { test: /\.woff2$/, loader: 'file-loader?name=[path][name].[ext]'}
    ]
  },

  plugins: [
    new ExtractTextPlugin('[name].css')
    //file : new ExtractTextPlugin('[path][name].css')
  ]
};
