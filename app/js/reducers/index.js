import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import { SPINNER } from '../actions';
import notes from './notes';
import pinControl from './pincontrol';

function spinner(state = false, action) {
  switch (action.type) {
    case SPINNER:
      return action.flag;
    default:
      return state;
  }
}

export default combineReducers({
  pinControl,
  notes,
  spinner,
  routing: routerReducer
});
