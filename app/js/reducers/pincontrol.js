import { SET_TEMP_PIN, SET_PIN } from '../actions';

export default function (store = { value: '', tempValue: '' }, action) {
  switch (action.type) {
    case SET_TEMP_PIN:
      return Object.assign({}, store, { tempValue: action.tempPin });
    case SET_PIN:
      return Object.assign({}, store, { value: action.pin, tempValue:'' })
    default:
      return store;
  }
}
