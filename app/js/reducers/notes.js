import {ADD_NOTE,CHANGE_NOTE,DELETE_NOTE,SET_NOTES} from '../actions';

function addNote (state, note) { return [...state, note]; };

function changeNote (state, note) {
  return state.map((n) => {
    if (n.id === note.id) return note;
    else return n;
  })
};

function deleteNote (state, id) { return [...state]; };

export default function (state = [], action) {
  switch (action.type) {
    case SET_NOTES:
      return action.list;
    case ADD_NOTE:
      return addNote(state, action.note);
    case CHANGE_NOTE:
      return changeNote(state, action.note);
    case DELETE_NOTE:
      return deleteNote(sate, action.id);
    default:
      return state;
  }
}
