import React from 'react';
import createHistory from 'history/createBrowserHistory';
import createStore from './store';
import { render } from 'react-dom';
import Root from './components/Root';

import { loadDataFromDB, setPin, backAction, spinner } from './actions';

let opt = void 0;
if (window.cordova) {
  opt = {
    basename: location.pathname
  }
}

const history = createHistory(opt);
const store = createStore(history);

store.dispatch(loadDataFromDB());

document.addEventListener("pause", () => {
  store.dispatch(setPin(''));
}, false);

document.addEventListener("backAction", (e) => {
  e.preventDefault();
  store.dispatch(backAction());
}, false);

//store.dispatch(setPin('123456'));

render(
  <Root store={store} history={history} />,
  document.getElementById('root')
);
