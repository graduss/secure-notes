import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';
import { routerMiddleware } from 'react-router-redux';
import sNotesApp from '../reducers';


export default function (history) {
  const middleware = routerMiddleware(history);
  const store = createStore(
    sNotesApp,
    applyMiddleware(middleware,thunkMiddleware,logger)
  );

  return store;
}

import Builder from 'indexeddb-promised';
const builder = new Builder('appDB');

export const DB = builder.setVersion(1)
.addObjectStore({
  name: 'notes',
  keyType: { autoIncrement: true, keyPath: 'id' }
})
.build();
