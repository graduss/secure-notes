import React, { Component } from 'react';

class NewNote extends Component {
  constructor () {
    super();
    this.titleNode = null;
    this.contentNode = null;
  }

  get data () {
    return {
      title : this.titleNode.value,
      content: this.contentNode.value
    }
  }

  render () {
    let { note = {title:'', content: ''} } = this.props;
    return (
      <div className='new-note'>
        <input type='text' className='title' ref={ node => this.titleNode = node } placeholder="Title" defaultValue={note.title} />
        <textarea className='content' ref={ node => this.contentNode = node } placeholder="Content" defaultValue={note.content} ></textarea>
      </div>
    )
  }
}

export default NewNote;
