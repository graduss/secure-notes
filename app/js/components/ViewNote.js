import React, { Component } from 'react';

const ViewNote = ({note = { title:'', content: '' }}) => {
  return (
    <div className='view-note'>
      <h2 className='note-title'>{ note.title }</h2>
      <div className='note-content'>{ note.content }</div>
    </div>
  )
}

export default ViewNote;
