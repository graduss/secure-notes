import React from 'react';
import { connect } from 'react-redux';
import { setPin } from '../actions';

const UnsetPin = connect(
  void 0,
  (dispatch) => ({
    unsetPin: () => dispatch(setPin(''))
  })
)(({unsetPin}) => <button className='unset-pin' onClick={() => setTimeout(() => unsetPin(), 200)} />)

export default UnsetPin;
