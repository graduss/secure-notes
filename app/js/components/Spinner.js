import React, { Component } from 'react';
import { connect } from 'react-redux';

const Spinner = ({spinner}) => {
  if (spinner) return <div className='spinner' />
  else return null;
}

export default connect(
  (store) => ({spinner: store.spinner})
)(Spinner);
