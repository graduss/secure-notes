import React, { Component } from 'react';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { AES, enc } from 'crypto-js';
import { backAction } from '../actions';
import { saveNote } from '../actions';
import ViewNote from './ViewNote';
import NewNote from './NewNote';
import Header from './Header';
import Footer from './Footer';
import UnsetPin from './UnsetPin';

class NoteScreen extends Component {
  get noteId () { return this.props.note.id }

  renderButtons () {
    let { action, note, goEdit } = this.props;

    if (action === 'view') {
      return (<buttin className='edit-note' onClick={() => goEdit(note.id)} />)
    } else if (action === 'edit') {
      return (<buttin className='save-note' onClick={() => this.saveNote()} />)
    }
  }

  saveNote () {
    let { saveNote } = this.props;
    saveNote(Object.assign(this.Note.data, { id:this.noteId }));
  }

  renderContainer () {
    let { action, note } = this.props;

    if (action === 'view') {
      return( <ViewNote note={note} /> );
    } else if (action === 'edit') {
      return( <NewNote ref={inst => this.Note = inst} note={note} /> );
    }
  }

  render () {
    let { note } = this.props;

    return (
      <div className="app-layout">
        <Header />

        { this.renderContainer() }

        <Footer>
          { this.renderButtons() }
          <UnsetPin />
        </Footer>
      </div>
    )
  }

  static getStore (store, { match: { params: { id, action } } }) {
    return {
      note: NoteScreen.decryptNote(
        store.notes.find((note) => note.id == id),
        store.pinControl.value
      ),
      action,
      id
    }
  }

  static getActions (dispatch) {
    return {
      goEdit: (id) => dispatch(push(`/note/edit/${id}`)),
      saveNote: (id,data) => dispatch(saveNote(id,data))
    }
  }

  static decryptNote (note,pin) {
    if (note && pin) {
      try {
        return Object.assign({}, note, {
          title: AES.decrypt(note.title, pin).toString(enc.Utf8),
          content: AES.decrypt(note.content, pin).toString(enc.Utf8)
        });
      } catch (e) {
        console.error(`DECRYPT: for note id ${note.id}`);
        return null;
      }
    } else null;
  }
}

export default connect(
  NoteScreen.getStore,
  NoteScreen.getActions,
)(NoteScreen);
