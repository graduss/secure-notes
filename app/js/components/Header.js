import React, { Component } from 'react';
import Back from './BackButton'

class Header extends Component {
  render () {
    let { children } = this.props;
    return (
      <header>
      <Back />
      {children}
      </header>
    )
  }
}

export default Header;
