import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import NoteList from './NoteList';
import { AES,enc } from 'crypto-js';

function decryptNote(note, pin) {
  if (pin) {

    try {
      return Object.assign({}, note, {
        title: AES.decrypt(note.title, pin).toString(enc.Utf8),
        content: AES.decrypt(note.content, pin).toString(enc.Utf8)
      })
    } catch (e) {
      console.error(`DECRYPT: for note id ${note.id}`);
      return null;
    }

  } else return null;
}

function mapStateToProps (store, { categoryId }) {
  return {
    notes : store.notes.map((note) => decryptNote(note, store.pinControl.value))
            .filter((note) => note && (note.title || note.content))
  };
}

function mapDispatchToProps (dispatch) {
  return {
    openNpte: (id) => { dispatch(push(`/note/view/${id}`)) }
  };
}

const VisibleNotes = connect(
  mapStateToProps,
  mapDispatchToProps
)(NoteList)

export default VisibleNotes;
