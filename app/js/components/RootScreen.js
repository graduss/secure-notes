import React from 'react';
import { connect } from 'react-redux';
import { editNoteList, edirCategoryList,setPin } from '../actions';

import Header from './Header';
import Footer from './Footer';
import VisibleNotes from './VisibleNotes';
import AddNewNote from './AddNewNote';
import UnsetPin from './UnsetPin';

import MenuComponent from './Menu';
const Menu = connect(
  void 0,
  (dispatch) => ({
    actions : {
      editNoteLixt: () => dispatch(editNoteList()),
      editCategoryList: () => dispatch(edirCategoryList())
    }
  })
)(MenuComponent);

const RootScreen = () => (
  <div className="app-layout">
    <Header>
      <Menu />
    </Header>

    <VisibleNotes />

    <Footer>
      <AddNewNote />
      <UnsetPin />
    </Footer>
  </div>
)

export default RootScreen;
