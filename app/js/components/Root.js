import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Route } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';

import RootScreen from './RootScreen';
import NewNoteScreen from './NewNoteScreen';
import NoteScreen from './NoteScreen';
import PinControl from './PinControl';
import Spinner from './Spinner';

const Root = ({ store, history }) => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div className='root-app'>
        <Route exact path="/" component={RootScreen} />
        <Route path="/note/new" component={NewNoteScreen} />
        <Route path="/note/:action/:id" component={NoteScreen} />

        <PinControl />
        <Spinner />
      </div>
    </ConnectedRouter>
  </Provider>
);

Root.propTypes = {
  history: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
};

export default Root;
