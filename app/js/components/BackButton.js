import React, { Component } from 'react';
import { connect } from 'react-redux';
import { backAction } from '../actions';

const back = ({ backAction }) => {
  return (<div className='back-wrap'>
    <a className='back-button' onClick={() => backAction()}></a>
  </div>)
}

export default connect(
  void 0,
  (dispatch) => ({ backAction: () => dispatch(backAction()) })
)(back)
