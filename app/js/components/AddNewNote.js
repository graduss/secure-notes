import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

let AddNewNote = ({ dispatch }) => (
  <button className="add-new" onClick={() => setTimeout(() => dispatch(push('/note/new'), 1000))} />
)

AddNewNote = connect()(AddNewNote);

export default AddNewNote;
