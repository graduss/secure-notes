import React, { Component } from 'react';
import { connect } from 'react-redux';

import { saveNewNote } from '../actions';

import NewNote from './NewNote';
import Header from './Header';
import Footer from './Footer';
import UnsetPin from './UnsetPin';

class NewNoteScreen extends Component {
  saveNewNote () {
    let { dispatch } = this.props;
    dispatch(saveNewNote(this.Node.data))
  }

  render () {
    return (
      <div className="app-layout">
        <Header>
        </Header>

        <NewNote ref={inst => this.Node = inst} />

        <Footer>
          <buttin className='save-note' onClick={() => this.saveNewNote()} />
          <UnsetPin />
        </Footer>
      </div>
    )
  }
}

export default connect()(NewNoteScreen);
