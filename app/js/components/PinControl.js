import React, { Component } from 'react';
import { connect } from 'react-redux';
import { pinControl, enterPin, setPin, setTemp, closeApp } from '../actions';

class Status extends Component {
  renderStatus (pinSize, value) {
    let list = [];

    for ( let i = 0; i < pinSize; i++ ) {
      if (i < value.length) {
        list.push(<div key={i} className='statu-fill' />)
      } else list.push(<div key={i} />)
    }

    return list;
  }

  render () {
    let { pinSize, value } = this.props;
    return (
      <div className='pin-status'>
        { this.renderStatus(pinSize, value) }
      </div>
    )
  }
}

class KeyBord extends Component {
  onPress (e) {
    let { onPress } = this.props;

    if (e.target.className === 'pin-butoon') {
      onPress(e.target.dataset.num);
    }

    navigator.vibrate(5);
  }

  render () {

    return (
      <div className='pin-keyboard' onClick={(e) => this.onPress(e)}>
        <button className='pin-butoon' data-num='1'></button>
        <button className='pin-butoon' data-num='2'></button>
        <button className='pin-butoon' data-num='3'></button>
        <button className='pin-butoon' data-num='4'></button>
        <button className='pin-butoon' data-num='5'></button>
        <button className='pin-butoon' data-num='6'></button>
        <button className='pin-butoon' data-num='7'></button>
        <button className='pin-butoon' data-num='8'></button>
        <button className='pin-butoon' data-num='9'></button>
        <button className='pin-butoon' data-num='0'></button>
      </div>
    )
  }
}

class PinContriol extends Component {
  constructor () {
    super();
    this.pinSize = 6;
  }
  updateTempValue (num) {
    let { store: { tempValue }, setTempPin, enterPin } = this.props;
    let newTempValue = `${tempValue}${num}`;

    if ( newTempValue.length < this.pinSize ) {
      setTempPin(newTempValue)
    } else {
      enterPin(newTempValue)
    }
  }

  renderButton () {
    let { store: { value, tempValue }, clearPin, cansel } = this.props;
    if (tempValue) {
      return (<buttom onClick={() => {
        clearPin();
        navigator.vibrate(5);
      }}>Clean</buttom>)
    } else {
      return (<buttom onClick={() => {
        cansel();
        navigator.vibrate(5);
      }}>Cansel</buttom>)
    }
  }

  render () {
    let { store: { value, tempValue }, enterPin, setTempPin, clearPin, unsetPin } = this.props;

    if (value) {
      return null;
    } else {
      return (
        <div className='enter-pin-wrap'>
          <div className='enter-pin'>
            <Status pinSize={this.pinSize} value={tempValue} />
            <KeyBord onPress={num => this.updateTempValue(num)} />
            <div className='button-wrap'>{ this.renderButton() }</div>
          </div>
        </div>
      )
    }
  }
}

export default connect(
  (store) => ({ store: store.pinControl }),
  (dispatch) => ({
    enterPin (pin) { dispatch(enterPin(pin)) },
    setTempPin (pin) { dispatch(setTemp(pin)) },
    clearPin () { dispatch(setTemp('')) },
    unsetPin () { dispatch(setPin('')) },
    cansel () { dispatch(closeApp()) }
  })
)(PinContriol)
