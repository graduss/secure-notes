import React, { Component } from 'react';

class Footer extends Component {
  render () {
    let { children } = this.props;
    return (
      <footer>{children}</footer>
    )
  }
}

export default Footer;
