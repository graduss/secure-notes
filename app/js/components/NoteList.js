import React, { Component } from 'react';

class Note extends Component {
  render () {
    let { note, onClick } = this.props;

    return (
      <div className='note-item' onClick={(e) => setTimeout(() => onClick(e), 200)}>
        <h2>{note.title}</h2>
        <article>{note.content}</article>
      </div>
    )
  }
}

class NoteList extends Component {
  render () {
    let { notes, openNpte } = this.props;
    return (
      <div className='note-list'>
      {
        notes.map((note) => {
          return(<Note key={note.id} note={note} onClick={() => openNpte(note.id)} />)
        })
      }
      </div>
    );
  }
}

export default NoteList;
