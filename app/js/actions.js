import { push, goBack } from 'react-router-redux';
import { DB } from './store';
import { SHA256, AES } from 'crypto-js';

export const SET_PIN = 'SET_PIN';
export const SET_TEMP_PIN = 'SET_TEMP_PIN';
export const ADD_NOTE = 'ADD_NOTE';
export const CHANGE_NOTE = 'CHANGE_NOTE';
export const DELETE_NOTE = 'DELETE_NOTE';
export const SPINNER = 'SPINNER';
export const SET_NOTES = 'SET_NOTES';
export const EDIT_NOTE_LIST = 'EDIT_LIST';
export const EDIT_CATEGORY_LIST = 'EDIT_CATEGORY_LIST';

export function error(error) {
  return (dispatch) => {
    if (error instanceof Error) {
      alert(error.message);
    } else alert(error);
  }
}

export function backAction () {
  return (dispatch, getStore) => {
    let { routing: { location: { pathname } } } = getStore();

    if ( pathname === '/' ) return dispatch(closeApp());
    else if (/^\/note\/view/.test(pathname) ) return dispatch(push('/'));
    else return dispatch(goBack());
  }
}

export function editNoteList () {
  return {
    type: EDIT_NOTE_LIST
  }
}

export function setTemp(tempPin) {
  return {
    type: SET_TEMP_PIN,
    tempPin
  }
}

export function setPin(pin) {
  return {
    type: SET_PIN,
    pin
  }
}

export function enterPin(pin) {
  return (dispatch, getStore) => {
    dispatch(spinner(true));

    if (localStorage.getItem('PIN-ENCRYPT')) {
      if ( checkPin(pin) ) {
        dispatch(setPin(pin));
      } else {
        dispatch(setTemp(''));
        dispatch(error('Pin is incorrect'));
      }
    } else {
      setPinEncrypt(pin);
      dispatch(setPin(pin));
    }

    dispatch(spinner(false));
  }
}

function setPinEncrypt (pin) {
  localStorage.setItem('PIN-ENCRYPT', SHA256(pin).toString());
}

function checkPin (pin) {
  let pinEnctipt = localStorage.getItem('PIN-ENCRYPT');
  return pin && SHA256(pin).toString() === pinEnctipt;
}

export function closeApp() {
  return () => {
    navigator.app.exitApp();
  }
}

export function spinner(flag){
  return {
    type: SPINNER,
    flag
  }
}

export function addNote (note) {
  return {
    type: ADD_NOTE,
    note
  }
}

export function changeNote (note) {
  return {
    type: CHANGE_NOTE,
    note
  }
}

export function edirCategoryList () {
  return {
    type : EDIT_CATEGORY_LIST
  }
}

function setNotes (list) {
  return {
    type: SET_NOTES,
    list
  }
}

export function loadDataFromDB () {
  return (dispatch) => {
    dispatch(spinner(true));

    return DB.notes.getAll()
    .then((notes) => dispatch(setNotes(notes)))
    .catch((e) => console.error(e))
    .then(() => dispatch(spinner(false)));
  }
}

export function saveNewNote (note) {
  return (dispatch, getStore) => {
    let { pinControl:{value:pin} } = getStore();
    dispatch(spinner(true));

    let n = Object.assign({}, note, {
      title: AES.encrypt(note.title, pin).toString(),
      content: AES.encrypt(note.content, pin).toString()
    })

    return DB.notes.add(n)
    .then((id) => {
      dispatch(addNote(Object.assign(n,{id})));
      dispatch(push('/'));
    })
    .catch((e) => {
      console.error(e);
    })
    .then(() => dispatch(spinner(false)));
  }
}

export function saveNote (note) {
  return (dispatch, getStore) => {
    let { pinControl:{value:pin} } = getStore();
    dispatch(spinner(true));

    let n = Object.assign({}, note, {
      title: AES.encrypt(note.title, pin).toString(),
      content: AES.encrypt(note.content, pin).toString()
    });

    return DB.notes.put(n)
    .then(() => {
      dispatch(changeNote(n));
      dispatch(push(`/note/view/${n.id}`));
    })
    .catch((e) => console.error(e))
    .then(() => dispatch(spinner(false)));
  }
}
